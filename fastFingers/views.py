from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from .forms import LoginForm, RegisterForm
from .models import Party


def index(request):
    return redirect('/fastFingers/login/')


def home_view(request):
    party_list = Party.objects.order_by('speed').reverse()

    accuracy_list = [a.accuracy for a in party_list if a.user_id == request.user.id]
    if (len(accuracy_list) > 0):
        avg_acc = round(sum(accuracy_list) / len(accuracy_list), 1)
    else:
        avg_acc = 0

    keyNumber_list = [a.keyNumber for a in party_list if a.user_id == request.user.id]
    if (len(keyNumber_list) > 0):
        avg_keyNb = round(sum(keyNumber_list) / len(keyNumber_list), 1)
    else:
        avg_keyNb = 0

    speed_list = [a.speed for a in party_list if a.user_id == request.user.id]
    if (len(speed_list) > 0):
        avg_speed = round(sum(speed_list) / len(speed_list), 1)
    else:
        avg_speed = 0

    time_list = [a.time for a in party_list if a.user_id == request.user.id]
    if (len(time_list) > 0):
        avg_time = round(sum(time_list) / len(time_list), 1)
    else:
        avg_time = 0

    wordNumber_list = [a.wordNumber for a in party_list if a.user_id == request.user.id]
    if (len(wordNumber_list) > 0):
        avg_wordNb = round(sum(wordNumber_list) / len(wordNumber_list), 1)
    else:
        avg_wordNb = 0

    errorNumber_list = [a.errorNumber for a in party_list if a.user_id == request.user.id]
    if (len(errorNumber_list) > 0):
        avg_error = round(sum(errorNumber_list) / len(errorNumber_list), 1)
    else:
        avg_error = 0

    context = {
        'party_list': party_list,
        'avg_acc': avg_acc,
        'avg_keyNb': avg_keyNb,
        'avg_speed': avg_speed,
        'avg_time': avg_time,
        'avg_wordNb': avg_wordNb,
        'avg_error': avg_error,

    }
    return render(request, 'fastFingers/home.html', context)


def login_view(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)

            return redirect('/fastFingers/home/')

    else:
        return render(request, 'fastFingers/login.html')


def logout_view(request):
    logout(request)
    return redirect('/fastFingers/login/')


def register_view(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():

            if (form.cleaned_data['password'] != form.cleaned_data['confirm_password']):
                return render(request, 'fastFingers/register.html')

            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            User.objects.create_user(username=username, password=password)
            return redirect('/fastFingers/login/')

    return render(request, 'fastFingers/register.html')


def game_view(request):
    return render(request, 'fastFingers/game.html')


def result_view(request, time, errorNumber, wordNumber, keyNumber, accuracy, speed):
    Party.objects.create(
        time=time, errorNumber=errorNumber, wordNumber=wordNumber,
        keyNumber=keyNumber, accuracy=accuracy, speed=speed,
        user_id=request.user.id
    )

    return redirect('/fastFingers/home/')
