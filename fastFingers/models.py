from django.contrib.auth.models import User
from django.db import models


class Party(models.Model):
    accuracy = models.PositiveIntegerField()
    speed = models.PositiveIntegerField()
    keyNumber = models.PositiveIntegerField()
    wordNumber = models.PositiveIntegerField()
    errorNumber = models.PositiveIntegerField()
    time = models.PositiveIntegerField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
